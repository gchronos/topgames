import {browser} from 'protractor';

import {homePage} from './home-page';


describe('First Page', () => {

  beforeAll(async () => {
    await browser.get('');
    homePage.getStarted();
  });

  it('should have a Log in button and Register link', async () => {
    expect(homePage.getNavLinkTop.isPresent()).toBeTruthy();
  });

  it('should have correct text for Get Started button', async () => {
    expect(homePage.getNavLinkTop.getText()).toContain('Get Started');
  });
});
