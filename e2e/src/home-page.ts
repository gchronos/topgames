import {$} from 'protractor';

import {click, waitFor} from './utils';


/**
 * Home page definition.
 */
class HomePage {
  // UI element declarations.
  // Note that element declarations are getter functions.
  // This allows lazy evaluation of locators which is neccessary to avoid evaluating them
  // too early and failing. By making them functions the locators are evaluated
  // exactly when they are needed in the test instead of before the tests.
  //
  // Note: if there is a no reliable good CSS selector on the UI element that you
  // need to locate in tests the best practice is to add a data-test-id attribute
  // in the html and use it here, like it is done for loginBth and registerLink.
  get getNavLinkTop() { return $(`app-root app-header [data-e2eid="top"]`); }



  // Operations
  async getStarted() {
    await waitFor(this.getNavLinkTop);
    await click(this.getNavLinkTop);
  }
}

export const homePage = new HomePage();
