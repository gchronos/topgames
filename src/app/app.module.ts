import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';

import {GameItemComponent} from './components/game-item/game-item.component';
import {LoadingComponent} from './components/loading/loading.component';
import {HeaderComponent} from './components/header/header.component';
import {MaterialModule} from '@app/modules/material.module';
import {AppRoutingModule} from '@app/app-routing.module';
import {StoreModule} from '@app/modules/store.module';
import {AppDirectives} from '@app/core/directives';
import {AppComponent} from '@app/app.component';
import {AppPipes} from '@app/core/pipes';


@NgModule({
  declarations: [
    AppComponent,
    ...AppPipes,
    ...AppDirectives,
    HeaderComponent,
    LoadingComponent,
    GameItemComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MaterialModule,
    StoreModule
  ],
  providers: [
    ...AppPipes
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
