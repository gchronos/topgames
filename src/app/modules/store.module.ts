import {NgxsDispatchPluginModule} from '@ngxs-labs/dispatch-decorator';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {NgxsModule} from '@ngxs/store';

import {environment} from '@env/environment';
import {AppState} from '@app/core/store/store.index';


@NgModule({
  imports: [
    CommonModule,
    NgxsModule.forRoot([...AppState], {
      developmentMode: !environment.production
    }),
    NgxsDispatchPluginModule.forRoot()
  ],
  exports: [
    NgxsModule,
    NgxsDispatchPluginModule,
  ]
})
export class StoreModule {
}
