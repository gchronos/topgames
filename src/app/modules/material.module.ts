import {NgModule} from '@angular/core';

import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatStepperModule} from '@angular/material/stepper';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatNativeDateModule} from '@angular/material/core';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import {MatRadioModule} from '@angular/material/radio';
import {MatBadgeModule} from '@angular/material/badge';
import {MatChipsModule} from '@angular/material/chips';
import {MatInputModule} from '@angular/material/input';
import {MatMenuModule} from '@angular/material/menu';
import {MatTabsModule} from '@angular/material/tabs';
import {MatListModule} from '@angular/material/list';


const matModules = [
    MatButtonModule,
    MatSelectModule,
    MatMenuModule,
    MatInputModule,
    MatSnackBarModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatTabsModule,
    MatSidenavModule,
    MatAutocompleteModule,
    MatDialogModule,
    MatButtonToggleModule,
    MatTooltipModule,
    MatChipsModule,
    MatDatepickerModule,
    MatRadioModule,
    MatPaginatorModule,
    MatStepperModule,
    MatNativeDateModule,
    MatListModule,
    MatBadgeModule,
    MatProgressSpinnerModule
];

@NgModule({
    imports: [...matModules],
    exports: [...matModules]
})
export class MaterialModule {
}
