import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {NgxsModule} from '@ngxs/store';

import {NavLinksService} from '@app/core/store/navlinks/nav-links.service';
import {NgxsDispatchPluginModule} from '@ngxs-labs/dispatch-decorator';
import {NavLink} from '@app/core/store/navlinks/nav-links.model';
import {GameCategory} from '@app/core/store/games/games.model';
import {AppState} from '@app/core/store/store.index';
import {HeaderComponent} from './header.component';


const navLinks = [
  {name: 'Top Games', id: GameCategory.top, categoryType: [GameCategory.top], isActive: false},
  {name: 'New Games', id: GameCategory.new, categoryType: [GameCategory.new], isActive: false},
  {name: 'Slots', id: GameCategory.slots, categoryType: [GameCategory.slots], isActive: false},
  {name: 'Jackpots', id: GameCategory.classic, categoryType: [GameCategory.classic], isActive: false},
  {name: 'Live', id: GameCategory.classic, categoryType: [GameCategory.classic], isActive: false},
  {name: 'Blackjack', id: GameCategory.blackjack, categoryType: [GameCategory.blackjack], isActive: false},
  {name: 'Roulette', id: GameCategory.roulette, categoryType: [GameCategory.roulette], isActive: false},
  {name: 'Table', id: GameCategory.classic, categoryType: [GameCategory.classic], isActive: false},
  {name: 'Poker', id: GameCategory.poker, categoryType: [GameCategory.poker], isActive: false},
  {name: 'Other', id: GameCategory.other, categoryType: [GameCategory.fun, GameCategory.ball, GameCategory.virtual], isActive: false}
];

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let compiled: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        NavLinksService
      ],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        NgxsModule.forRoot([...AppState]),
        NgxsDispatchPluginModule.forRoot()
      ],
      declarations: [HeaderComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    compiled = fixture.debugElement.nativeElement;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should have nav links`, () => {
    let headerButtonsTitles = '';

    compiled
      .querySelectorAll(`[data-e2eid="navLink`)
      .forEach((item: HTMLElement) => (headerButtonsTitles += item.textContent));

    navLinks.forEach((item: NavLink) => {
      expect(headerButtonsTitles).toContain(item.name);
    });
  });
});
