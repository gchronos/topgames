import {GamesService} from '@app/core/store/games/games.service';
import { Component, OnInit } from '@angular/core';

import {NavLinksService} from '@app/core/store/navlinks/nav-links.service';
import {GamesStateListItem} from '@app/core/store/games/games.model';
import {NavLink} from '@app/core/store/navlinks/nav-links.model';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  mobileMenuIsActive = false;

  constructor(
    public gamesService: GamesService,
    public navLinksService: NavLinksService
  ) {
    this.navLinksService.activeLink$.subscribe((activeLink: NavLink) => {
      this.gamesService.setActiveGamesList(activeLink);
    });
  }

  ngOnInit(): void {
  }

  onChangeTab(navLink: NavLink): void {
    this.navLinksService.setActiveNavLink(navLink);
    this.mobileMenuIsActive = false;
  }

  trackById(item: GamesStateListItem) {
    return item.id;
  }
}
