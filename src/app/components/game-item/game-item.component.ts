import {Component, Input} from '@angular/core';

import {GameCategory, GamesStateListItem} from '@app/core/store/games/games.model';
import {NavLink} from '@app/core/store/navlinks/nav-links.model';


@Component({
  selector: 'app-game-item',
  templateUrl: './game-item.component.html',
  styleUrls: ['./game-item.component.scss']
})
export class GameItemComponent {
  @Input() value: GamesStateListItem;
  @Input() set activeLink(link: NavLink) {
    const firstCategory = this.value.categories[0];

    if (link) {
      if (link.id === GameCategory.other) {
        this.ribbon = firstCategory;
      } else {
        this.ribbon = link.id;
      }
    } else {
      this.ribbon = firstCategory;
    }
  }

  ribbon: string;
}
