import {NgxsDispatchPluginModule} from '@ngxs-labs/dispatch-decorator';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {TestBed, async} from '@angular/core/testing';
import {NgxsModule, Store} from '@ngxs/store';

import {HeaderComponent} from '@app/components/header/header.component';
import {GamesState} from '@app/core/store/games/games.state';
import {GetGames} from '@app/core/store/games/games.actions';
import {AppComponent} from './app.component';



describe('AppComponent', () => {
  let store: Store;
  const gamesState = {
    games: {
      responseList: [
        {
          categories: [
            'top',
            'slots',
            'new'
          ],
          name: 'The Wish Master',
          image: '//stage.whgstage.com/scontent/images/games/NETHEWISHMASTER.jpg',
          id: 'NETHEWISHMASTER'
        },
        {
          categories: [
            'top',
            'slots',
            'new'
          ],
          name: 'Aliens',
          image: '//stage.whgstage.com/scontent/images/games/NEALIENS.jpg',
          id: 'NEALIENS'
        },
        {
          categories: [
            'top',
            'slots',
            'new'
          ],
          name: 'Starburst',
          image: '//stage.whgstage.com/scontent/images/games/NESTARBURST.jpg',
          id: 'NESTARBURST'
        },
        {
          categories: [
            'top',
            'slots',
            'new'
          ],
          name: 'Jack Hammer 2',
          image: '//stage.whgstage.com/scontent/images/games/NEJACKHAMMER2.jpg',
          id: 'NEJACKHAMMER2'
        }
      ]
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        NgxsModule.forRoot([GamesState]),
        NgxsDispatchPluginModule.forRoot()
      ],
      declarations: [
        AppComponent,
        HeaderComponent
      ],
    }).compileComponents();

    store = TestBed.inject(Store);
    store.reset(gamesState);
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should trigger GetGames action and get response', () => {
    store.dispatch(new GetGames());
    const responseList = store.selectSnapshot(state => state.games.responseList);
    expect(responseList).toBeDefined();
  });
});
