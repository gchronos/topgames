import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {GamesStateListItem} from '@app/core/store/games/games.model';
import {environment} from '@env/environment';


@Injectable({
  providedIn: 'root'
})
export class GamesApiService {
  private gamesPath = 'games.php';

  constructor(
    private http: HttpClient,
  ) {
  }

  getGames(): Observable<GamesStateListItem[]> {
    return this.http.get(environment.apiUrl + this.gamesPath) as Observable<GamesStateListItem[]>;
  }
}

