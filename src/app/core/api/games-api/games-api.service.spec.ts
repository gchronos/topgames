import { TestBed } from '@angular/core/testing';

import { GamesApiService } from './games-api.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('GamesApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    const service: GamesApiService = TestBed.get(GamesApiService);
    expect(service).toBeTruthy();
  });
});
