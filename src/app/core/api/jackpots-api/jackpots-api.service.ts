import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

import {environment} from '@env/environment';


@Injectable({
  providedIn: 'root'
})
export class JackpotsApiService {
  private jackpotsPath = 'jackpots.php';

  constructor(
    private http: HttpClient,
  ) {
  }

  getJackpots() {
    return this.http.get(environment.apiUrl + this.jackpotsPath);
  }
}

