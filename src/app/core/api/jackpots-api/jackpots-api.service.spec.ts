import { TestBed } from '@angular/core/testing';

import {JackpotsApiService} from '@app/core/api/jackpots-api/jackpots-api.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';


describe('GamesApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    const service: JackpotsApiService = TestBed.get(JackpotsApiService);
    expect(service).toBeTruthy();
  });
});
