import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {Injectable} from '@angular/core';


@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  constructor() {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      tap(evt => this.handleSuccessHttpResponse(evt)),
      catchError(err => this.handleErrorHttpResponse(err))
    );
  }

  private handleSuccessHttpResponse(evt): void {
  }

  private handleErrorHttpResponse(err: HttpErrorResponse): Observable<any> {
    return throwError(err);
  }
}

