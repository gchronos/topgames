import {JackpotsStateInterface} from '@app/core/store/jackpots/jackpots.model';
import {NavLinksStateInterface} from '@app/core/store/navlinks/nav-links.model';
import {GamesStateInterface} from '@app/core/store/games/games.model';

export interface AppState {
  games: GamesStateInterface;
  jackpots: JackpotsStateInterface;
  navLinks: NavLinksStateInterface;
}

export enum AppStateList {
  games = 'games',
  jackpots = 'jackpots',
  navLinks = 'navLinks',
}
