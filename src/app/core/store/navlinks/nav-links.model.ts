import {GameCategory} from '@app/core/store/games/games.model';

export interface NavLinksStateInterface {
  links: NavLink[];
  activeLink: NavLink;
}

export interface NavLink {
  name: string;
  id: GameCategory;
  categoryType: GameCategory[];
  isActive: boolean;
}
