import {NavLink} from '@app/core/store/navlinks/nav-links.model';

export class SetActiveNavLink {
  static readonly type = '[SetActiveNavLink]: action';

  constructor(public payload: NavLink) {}
}


export class SetActiveNavLinkFromParams {
  static readonly type = '[SetActiveNavLinkFromParams]: action';

  constructor(public payload: string) {}
}

