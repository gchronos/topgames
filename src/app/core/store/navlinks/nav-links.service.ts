import {Dispatch} from '@ngxs-labs/dispatch-decorator';
import {Injectable} from '@angular/core';
import {Select} from '@ngxs/store';
import {Observable} from 'rxjs';

import {SetActiveNavLink, SetActiveNavLinkFromParams} from '@app/core/store/navlinks/nav-links.actions';
import {NavLink} from '@app/core/store/navlinks/nav-links.model';
import {AppState} from '@app/core/store/store.model';


@Injectable({providedIn: 'root'})
export class NavLinksService {

  @Select((state: AppState) => state.navLinks.links)
  public links$: Observable<NavLink[]>;

  @Select((state: AppState) => state.navLinks.activeLink)
  public activeLink$: Observable<NavLink>;

  @Dispatch()
  public setActiveNavLink(navLink: NavLink) {
    return new SetActiveNavLink(navLink);
  }

  @Dispatch()
  public setActiveNavLinkFromParams(s: string) {
    return new SetActiveNavLinkFromParams(s);
  }
}
