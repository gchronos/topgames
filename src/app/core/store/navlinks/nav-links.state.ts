import {Action, State, StateContext} from '@ngxs/store';
import {ActivatedRoute, Router} from '@angular/router';
import {Injectable} from '@angular/core';

import {SetActiveNavLink, SetActiveNavLinkFromParams} from '@app/core/store/navlinks/nav-links.actions';
import {NavLink, NavLinksStateInterface} from './nav-links.model';
import {GameCategory} from '@app/core/store/games/games.model';
import {AppStateList} from '@app/core/store/store.model';
import {StringUtils} from '@app/core/utils/string.utils';


@Injectable()
@State<NavLinksStateInterface>({
  name: AppStateList.navLinks,
  defaults: {
    links: [
      {name: 'Top Games', id: GameCategory.top, categoryType: [GameCategory.top], isActive: false},
      {name: 'New Games', id: GameCategory.new, categoryType: [GameCategory.new], isActive: false},
      {name: 'Slots', id: GameCategory.slots, categoryType: [GameCategory.slots], isActive: false},
      {name: 'Jackpots', id: GameCategory.classic, categoryType: [GameCategory.classic], isActive: false},
      {name: 'Live', id: GameCategory.classic, categoryType: [GameCategory.classic], isActive: false},
      {name: 'Blackjack', id: GameCategory.blackjack, categoryType: [GameCategory.blackjack], isActive: false},
      {name: 'Roulette', id: GameCategory.roulette, categoryType: [GameCategory.roulette], isActive: false},
      {name: 'Table', id: GameCategory.classic, categoryType: [GameCategory.classic], isActive: false},
      {name: 'Poker', id: GameCategory.poker, categoryType: [GameCategory.poker], isActive: false},
      {name: 'Other', id: GameCategory.other, categoryType: [GameCategory.fun, GameCategory.ball, GameCategory.virtual], isActive: false}
    ],
    activeLink: null
  }
})
export class NavLinksState {

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {}


  @Action(SetActiveNavLink)
  setActiveNavLink({getState, patchState}: StateContext<NavLinksStateInterface>, {payload}: SetActiveNavLink) {
    if (payload.isActive) {
      // if user click again on the selected link - reset query params
      this.updateQueryParams();
    } else {
      // otherwise set it
      this.updateQueryParams(payload.name);
    }

    const state: NavLinksStateInterface = getState();
    let activeLink: NavLink = null;


    const links = state.links.map((item: NavLink) => {
      // reset all links
      item = {...item, isActive: false};

      if (StringUtils.removeSpacesAndLowercase(item.name) === StringUtils.removeSpacesAndLowercase(payload.name)) {
        // find and toggle current link
        item = {...item, isActive: !payload.isActive};

        // update active link
        activeLink = item.isActive ? item : null;
      }

      return item;
    });

    patchState({ links, activeLink });
  }

  @Action(SetActiveNavLinkFromParams)
  setActiveNavLinkFromParams({getState, patchState}: StateContext<NavLinksStateInterface>, {payload}: SetActiveNavLinkFromParams) {
    const state: NavLinksStateInterface = getState();
    let activeLink: NavLink = null;

    const links = state.links.map((item: NavLink) => {
      // reset all links
      item = {...item, isActive: false};

      if (StringUtils.removeSpacesAndLowercase(item.name) === StringUtils.removeSpacesAndLowercase(payload)) {
        item = {...item, isActive: true};

        // update active link
        activeLink = item;
      }

      return item;
    });


    patchState({ links, activeLink });
  }


  private updateQueryParams(q?: string) {
    this.router.navigate([''], {
      relativeTo: this.activatedRoute,
      queryParams: q ? { type: StringUtils.removeSpacesAndLowercase(q) } : null,
    });
  }
}
