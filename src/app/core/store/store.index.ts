import {NavLinksState} from '@app/core/store/navlinks/nav-links.state';
import {JackpotsState} from '@app/core/store/jackpots/jackpots.state';
import {GamesState} from '@app/core/store/games/games.state';

export const AppState = [
  GamesState,
  JackpotsState,
  NavLinksState
];
