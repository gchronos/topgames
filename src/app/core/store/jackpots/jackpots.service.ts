import {Dispatch} from '@ngxs-labs/dispatch-decorator';
import {Injectable} from '@angular/core';
import {Select} from '@ngxs/store';
import {Observable} from 'rxjs';

import {JackpotsStateListItem} from '@app/core/store/jackpots/jackpots.model';
import {GetJackpots} from '@app/core/store/jackpots/jackpots.actions';
import {AppState} from '@app/core/store/store.model';


@Injectable({providedIn: 'root'})
export class JackpotsService {

    @Select((state: AppState) => state.jackpots.list)
    public list$: Observable<JackpotsStateListItem[]>;

    @Dispatch()
    public getJackpots() {
        return new GetJackpots();
    }
}
