import {Action, State, StateContext} from '@ngxs/store';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

import {JackpotsStateInterface, JackpotsStateListItem} from '@app/core/store/jackpots/jackpots.model';
import {JackpotsApiService} from '@app/core/api/jackpots-api/jackpots-api.service';
import {GetJackpots} from '@app/core/store/jackpots/jackpots.actions';
import {AppStateList} from '@app/core/store/store.model';


@Injectable()
@State<JackpotsStateInterface>({
  name: AppStateList.jackpots,
  defaults: {
    list: [],
    loading: false,
    error: false
  }
})
export class JackpotsState {

  constructor(
    private http: HttpClient,
    private jackpotsApiService: JackpotsApiService,
  ) {}

  @Action(GetJackpots)
  getJackpots({patchState}: StateContext<JackpotsStateInterface>) {
    patchState({ loading: true });

    return this.jackpotsApiService.getJackpots().subscribe((res: JackpotsStateListItem[]) => {
      patchState({
        list: res,
        loading: false
      });
    }, () => {
      patchState({ error: true, loading: false });
    }, () => {
      patchState({ loading: false });
    });
  }
}
