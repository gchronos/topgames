export interface JackpotsStateInterface {
  list: JackpotsStateListItem[];
  loading: boolean;
  error: boolean;
}


export interface JackpotsStateListItem {
  game: string;
  amount: number;
}
