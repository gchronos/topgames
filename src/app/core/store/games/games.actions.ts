import {JackpotsStateListItem} from '@app/core/store/jackpots/jackpots.model';
import {NavLink} from '@app/core/store/navlinks/nav-links.model';


export class GetGames {
  static readonly type = '[GetGames]: action';
}

export class UpdateGameJackpots {
  static readonly type = '[UpdateGameJackpots]: action';

  constructor(public payload: JackpotsStateListItem[]) {}
}

export class SetActiveGamesList {
  static readonly type = '[SetActiveGamesList]: action';

  constructor(public payload?: NavLink) {}
}



