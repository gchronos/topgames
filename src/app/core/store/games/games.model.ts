export interface GamesStateInterface {
  responseList: GamesStateListItem[];
  activeList: GamesStateListItem[];
  events: GamesStateEvents;
}

export interface GamesStateListItem {
  // games server model
  categories: GameCategory[];
  name: string;
  image: string;
  id: string;

  // additional fields
  amount?: number;
}

export interface GamesStateEvents {
  loading: boolean;
  error: boolean;
}

export enum GameCategory {
  // games categories server model
  top = 'top',
  new = 'new',
  slots = 'slots',
  classic = 'classic',
  poker = 'poker',
  roulette = 'roulette',
  blackjack = 'blackjack',
  fun = 'fun',
  virtual = 'virtual',
  ball = 'ball',

  // additional fields
  other = 'other',
}


