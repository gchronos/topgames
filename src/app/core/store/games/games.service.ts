import {Dispatch} from '@ngxs-labs/dispatch-decorator';
import {Injectable} from '@angular/core';
import {Select} from '@ngxs/store';
import {Observable} from 'rxjs';

import {GetGames, SetActiveGamesList, UpdateGameJackpots} from '@app/core/store/games/games.actions';
import {GamesStateEvents, GamesStateListItem} from '@app/core/store/games/games.model';
import {JackpotsStateListItem} from '@app/core/store/jackpots/jackpots.model';
import {NavLink} from '@app/core/store/navlinks/nav-links.model';
import {AppState} from '@app/core/store/store.model';


@Injectable({providedIn: 'root'})
export class GamesService {

  @Select((state: AppState) => state.games.responseList)
  public responseList$: Observable<GamesStateListItem[]>;

  @Select((state: AppState) => state.games.activeList)
  public activeList$: Observable<GamesStateListItem[]>;

  @Select((state: AppState) => state.games.events)
  public events$: Observable<GamesStateEvents>;

  @Dispatch()
  public getGames() {
    return new GetGames();
  }

  @Dispatch()
  public updateGameJackpots(jackpots: JackpotsStateListItem[]) {
    return new UpdateGameJackpots(jackpots);
  }

  @Dispatch()
  public setActiveGamesList(navLink?: NavLink) {
    return new SetActiveGamesList(navLink);
  }
}
