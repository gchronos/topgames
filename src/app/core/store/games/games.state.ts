import {Action, State, StateContext, Store} from '@ngxs/store';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

import {GetGames, SetActiveGamesList, UpdateGameJackpots} from '@app/core/store/games/games.actions';
import {GamesStateInterface, GamesStateListItem} from '@app/core/store/games/games.model';
import {JackpotsStateListItem} from '@app/core/store/jackpots/jackpots.model';
import {NavLinksService} from '@app/core/store/navlinks/nav-links.service';
import {GamesApiService} from '@app/core/api/games-api/games-api.service';
import {AppState, AppStateList} from '@app/core/store/store.model';
import {NavLink} from '@app/core/store/navlinks/nav-links.model';
import {GamesService} from '@app/core/store/games/games.service';


@Injectable()
@State<GamesStateInterface>({
  name: AppStateList.games,
  defaults: {
    responseList: [],
    activeList: [],
    events: {
      loading: false,
      error: false
    }
  }
})
export class GamesState {

  constructor(
    private store: Store,
    private http: HttpClient,
    private gamesApiService: GamesApiService,
    private gamesService: GamesService,
    private navLinksService: NavLinksService
  ) {
  }


  @Action(GetGames)
  getGames({getState, patchState}: StateContext<GamesStateInterface>) {
    const state: GamesStateInterface = getState();
    patchState({ events: { ...state.events, loading: true } });

    // get url params
    const urlParams = new URLSearchParams(window.location.search);
    const paramsType = urlParams.get('type');

    return this.gamesApiService.getGames().subscribe((res: GamesStateListItem[]) => {
        // add amount field to games model
        const gamesList = res.map(item => ({...item, amount: null}));

        if (paramsType) {
          // if page was loaded with params - set active list based on active link
          this.navLinksService.setActiveNavLinkFromParams(paramsType);

          // get activeLink from state
          const activeLink = this.store.selectSnapshot<NavLink>((appState: AppState) => appState.navLinks.activeLink);

          patchState({
            responseList: gamesList,
            activeList: this.getActiveGamesList(gamesList, activeLink),
            events: { ...state.events, loading: false }
          });
        } else {
          patchState({
            responseList: gamesList,
            activeList: gamesList,
            events: { ...state.events, loading: false }
          });
        }
      }, () => {
        patchState({ events: {  error: true, loading: false } });
      });
  }


  @Action(SetActiveGamesList)
  setActiveGamesList({getState, patchState}: StateContext<GamesStateInterface>, {payload}: SetActiveGamesList) {
    const state: GamesStateInterface = getState();
    const listResponse = state.responseList;

    if (payload) {
      patchState({ activeList: this.getActiveGamesList(listResponse, payload) });
    } else {
      patchState({ activeList: listResponse });
    }
  }


  @Action(UpdateGameJackpots)
  updateGameJackpots({getState, patchState}: StateContext<GamesStateInterface>, {payload}: UpdateGameJackpots) {
    const state: GamesStateInterface = getState();
    const list = [...state.activeList];
    const listLen = list.length;
    let listIndex = 0;

    while (listIndex < listLen) {
      // while loop is the fastest loop if we working with a big arrays

      let payloadLen = payload.length;
      while (payloadLen--) {
        const curJackpot: JackpotsStateListItem = payload[payloadLen];

        if (list[listIndex].id === curJackpot.game) {
          list[listIndex] = {
            ...list[listIndex],
            amount: curJackpot.amount
          };
        }
      }

      listIndex++;
    }

    patchState({ activeList: list });
  }


  private getActiveGamesList(gamesList: GamesStateListItem[], navLink: NavLink): GamesStateListItem[] {
    const list = [];
    const gameResponseLen = gamesList.length;
    let gameIndex = 0;

    while (gameIndex < gameResponseLen) {
      // while loop is the fastest loop if we working with a big arrays
      const curGame = gamesList[gameIndex];

      let navLinkLen = navLink.categoryType.length;
      while (navLinkLen--) {
        const curCategory = navLink.categoryType[navLinkLen];

        if (curGame.categories.includes(curCategory)) {
          list.push(curGame);
        }
      }

      gameIndex++;
    }

    return list;
  }
}
