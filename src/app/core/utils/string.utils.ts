export class StringUtils {
  static removeSpacesAndLowercase(s: string): string {
    return s.replace(/\s/g, '').toLowerCase();
  }
}
