import {AfterViewInit, Component, OnInit} from '@angular/core';

import {JackpotsStateListItem} from '@app/core/store/jackpots/jackpots.model';
import {NavLinksService} from '@app/core/store/navlinks/nav-links.service';
import {JackpotsService} from '@app/core/store/jackpots/jackpots.service';
import {GamesStateListItem} from '@app/core/store/games/games.model';
import {GamesService} from '@app/core/store/games/games.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {

  constructor(
    public gamesService: GamesService,
    public jackpotsService: JackpotsService,
    public navLinksService: NavLinksService,
  ) {
  }

  ngOnInit(): void {
    // App initialization
    this.gamesService.getGames();

    this.jackpotsService.list$.subscribe((jackpots: JackpotsStateListItem[]) => {
      this.gamesService.updateGameJackpots(jackpots);
    });
  }

  ngAfterViewInit(): void {
    // update jackpots every 5 seconds
    setInterval(() => this.jackpotsService.getJackpots(), 5000);
  }

  trackById(item: GamesStateListItem): string {
    return item.id;
  }
}
